# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'email-tidier/version'

Gem::Specification.new do |spec|
  spec.name          = "email-tidier"
  spec.version       = Middleman::EmailTidier::VERSION
  spec.authors       = ["Adam Dawkins"]
  spec.email         = ["adam.dawkins@mintvelvet.co.uk"]
  spec.description   = "Tidy up email newsletter HTML"
  spec.summary       = "A middleman extension for producing better email HTML"
  spec.homepage      = "https://bitbucket.org/mintvelvet/email-tidier"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  #spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"

  spec.add_runtime_dependency "middleman", [">= 3.0.0"]
end
