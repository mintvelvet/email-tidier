require 'minitest/autorun'

#optionally require turn for minitest output
begin; require 'turn/autorun'; rescue LoadError; end
Turn.config.format = :outline
require File.expand_path('../../lib/email-tidier.rb', __FILE__)

