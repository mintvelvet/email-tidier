# Email::Tidier

MV Email tidier is a (middleman)[http://www.middlemanapp.com] extension for tidying the HTML output of email newsletters generated using (mailrox)[http://www.miailrox.com]

## Installation

Add this line to your middleman application's Gemfile:

    gem 'mv-email-tidier'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install mv-email-tidier

## Usage

TODO: Write usage instructions here
